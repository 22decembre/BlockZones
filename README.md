BlockZones Project
==================

English 
-------

Project to blacklist bad domains names, and bad adresses IP, *knew for bad activities*:
- ADS servers
- malwares, trackers, and others badlies
- bogons red

=> Please, see README_en.md

Français
--------

Projet pour blacklister des noms de domaines, et des adresses ip, *connus pour leur activité suspecte*, relatifs :
- serveurs ADS - publicitaires
- malwares, trackers, et autres "méchancetés".
- réseaux "bogons"
- et autres "badips" ...

=> Veuillez lire le fichier README_fr.md
