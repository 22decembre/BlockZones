########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
#	This file is part of "BlackLabel :: BlockZones Project"
#
# Date: 2017/08/08
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

########################################################################
###
##
#    Functions
##
###
########################################################################

# Create uniq list file by datas into array blocklist
mng_lists_bg() {

	[ "${debug}" -eq 1 ] && printf "%s \n" "### ${ttl_manager}: ${txt_read_array_bl}" >> "${bz_log}"
    [ "${verbose}" -eq 1 ] && display_mssg "#" "${txt_read_array_bl}"

    count="${#lists[@]}"

    if [ "${count}" -gt 0 ]; then

        for url in "${lists[@]}"; do

			if [[ "${url}" == *"bz_bogons"* ]]; then
				file=""
				filename="${DIR_SRC}/${url}"
			else
				file="$(echo "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
				filename="${DIR_DL}/${file}"
			fi
			
            if [ "${debug}" -eq 1 ]; then
				printf "file: %s \n" "${file}" >> "${bz_log}"
				printf "filename: %s \n" "${filename}" >> "${bz_log}"
				printf "nb seconds: %s \n" "${seconds}" >> "${bz_log}"
			fi

            if [ -f "${filename}" ]; then

                # get file seconds stat
                file_seconds=$(stat -f "%m" -t "%s" "${filename}")

                # calcul diff time in seconds
                diff_sec=$(echo "${timestamp} - ${file_seconds}" | bc)

                unset file_seconds

                if [ ${diff_sec} -gt ${seconds} ]; then

                    if [[ "${url}" != *"bz_bogons"* ]]; then download; fi

                fi

            else

                if [[ "${url}" != *"bz_bogons"* ]]; then download; fi

            fi
            
            if [ -f "${filename}" ]; then
				
				purge_files
				
				make_uniq_list
				
            fi 

            unset filename

        done

    else
		[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_no_data}" >> "${bz_log}"
        display_mssg "KO" "${txt_error_no_data}"
        byebye

    fi

    unset count

}
