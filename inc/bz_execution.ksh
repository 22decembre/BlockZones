########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
#	This file is part of "BlackLabel :: BlockZones Project"
#
# Date: 2017/08/25
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

. "${DIR_INC}/commons_functions.ksh"

if [ $dialog -eq 1 ]; then
	[[ "$0" == *"menu"* ]] && menu_dialog
	. "${DIR_INC}/${choice}_dialog.ksh"
else
	[[ "$0" == *"menu"* ]] && menu
	. "${DIR_INC}/${choice}.ksh" 
fi
    
########################################################################
###
##
#    EXECUTION: DO NOT TOUCH!
##
###
########################################################################

[[ "$0" != *"menu"* ]] && display_welcome

check_needed_softs

del_files

build_lists

mng_lists

[ "${choice}" == "blacklists" ] && build_uniq_list

transformer

create_one_sums

install_service

display_mssg_end
