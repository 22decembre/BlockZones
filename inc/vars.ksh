########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
#	This file is part of "BlackLabel :: BlockZones Project"
#
# Date: 2017/08/25
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

### DO NOT TOUCH!
OLD_TERM="$(echo $TERM)"
export TERM=xterm

### Project Name; DO NOT TOUCH!
NAME="BlockZones"

### Detect Ksh Version; DO NOT TOUCH!
VKSH="$(echo $KSH_VERSION)"

### Declare directories; DO NOT TOUCH!
DIR_DL="${ROOT}/downloads"
DIR_INC="${ROOT}/inc"
DIR_LANG="${ROOT}/lang"
DIR_LISTS="${ROOT}/lists"
DIR_LOG="${ROOT}/log"
DIR_SRC="${ROOT}/src"

########################################################################
### include configurables variables file ###############################
if [ -f "${DIR_INC}/cfg_vars.local" ]; then
	. "${DIR_INC}/cfg_vars.local"
else
	. "${DIR_INC}/cfg_vars.ksh"
fi
########################################################################

# disable few variables segun conditions...
if [ $cron -eq 1 ]; then 
	dialog=0
	use_color=0;
	verbose=0
fi

### destination directories services
dir_dest_bind="/etc/bind"
dir_dest_host="/etc"
dir_dest_unbound="/var/unbound/etc"

### Signify files 
dir_pub_key_signify="${DIR_INC}/BlockZones_GenLists.pub"
dir_sec_key_signify="${DIR_INC}/BlockZones_GenLists.sec"

### to detect tools; DO NOT TOUCH!
typeset -i use_curl=0
typeset -i use_wget=0

### declare OS variables; DO NOT TOUCH!
OSN="$(uname -s)"	# Get Operating System Name
OSR="$(uname -r)"	# Get Operating System Release

### declare color variables; DO NOT TOUCH!
if [ $use_color -eq 1 ]; then
	bold="$(tput bold)"
	dim="$(tput dim)"
	green="$(tput setaf 2)"
	neutral="$(tput sgr0)"
	red="$(tput setaf 1)"
fi

### others variables; DO NOT TOUCH!
choice=""	# variable choice to value menu
choice_bl=""
list=""		# list name
bz_log=""	# filenamge log; use by debug option
output=""	# filename for blacklists output; DO NOT TOUCH!

### dates; DO NOT TOUCH
now="$(date +"%x %X")"
today="$(date +'%Y-%m-%d_%H-%M-%S')"
timestamp="$(date +%s)"

typeset -i seconds=86400   #  default delay, in seconds, before downloads list, again. Modify only with precautions!

### lang; DO NOT TOUCH!
lang="$(echo ${LANG})"
[ -z "${lang}" ] && lang="$(echo ${LC_MESSAGES})"
[ -z "${lang}" ] && lang="en"
lang="$(echo "${lang}" | awk '{ print substr($0,0,2) }')"
#[[ "$VKSH" == *"PD KSH"* ]] && lang="$(printf '%.2s\n' "${lang}")" || lang="${lang:0:2}"	# substring two first caracters

# few arrays; DO NOT TOUCH!
set -A lists
set -A FILES
set -A menus 'badips' 'blacklists' 'bogons' 'quit'
set -A menus_blacklists 'unbound' 'bind8' 'bind9' 'hosts' 'pf'

########################################################################

if [[ "$0" == *"blacklists"* ]]; then
	ARG="${1-ARG}"
	[[ "$ARG" = "ARG" ]] && choice_bl="unbound"
	[[ ${menus_blacklists[@]} != *"$ARG"* ]] && choice_bl="unbound" || choice_bl="${ARG}"
fi
