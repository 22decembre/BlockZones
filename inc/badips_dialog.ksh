########################################################################
###
##
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified 2 Clauses
#
# Github: https://framagit.org/BlackLists/BlockZones.git
#
#	This file is part of "BlackLabel :: BlockZones Project"
#
# Date: 2017/08/08
#
##
###
########################################################################
###
##
#   For *BSD: ksh, ksh93...
##
###
########################################################################

########################################################################
###
##
#    Functions
##
###
########################################################################

# Create uniq list file by datas into array blocklist
mng_lists_bi4d() {
	
	[ "${debug}" -eq 1 ] && printf "%s \n" "### ${ttl_manager}: ${txt_read_array_bl}" >> "${bz_log}"
    dialog --backtitle "${ttl_project_name}" --title "${ttl_manager}" --infobox "${txt_read_array_bl}" 10 100

    count="${#lists[@]}" 
	modulo=$(echo "100/$count"|bc)
	
	[ "${debug}" -eq 1 ] && printf "%s %s | %s \n" "# manager:" "count: $count" "modulo: $modulo" >> "${bz_log}"

    if [ "${count}" -gt 0 ]; then
    
    	(
				
		#let counter+=$modulo
		#[ $counter -ge 100 ] && break

		for url in "${lists[@]}"; do

			ndd="$(echo "${url}" | awk -F'/' '{ print $3 }')"
			file="$(echo "${url##*/}" | sed -e 's#\?#_#g;s#=#_#g;s#php#txt#g;s#\&#_#g')";
			name="${ndd}_${file}"
			filename="${DIR_DL}/${name}"
			
			if [ "${debug}" -eq 1 ]; then
				printf "domain name: %s \n" "${ndd}" >> "${bz_log}"
				printf "file: %s \n" "${file}" >> "${bz_log}"
				printf "filename: %s \n" "${filename}" >> "${bz_log}"
			fi
				
			let x+=$modulo
				
			cat <<EOF
XXX
$x
$txt_mng_list $list ($x%)
$txt_download $url
$txt_transform $name
XXX
EOF

			# define seconds before new dl
			case "${ndd}" in
				"feeds.dshield.org") seconds=259200 ;;	# 3 days
				"lists.blocklist.de") seconds=172800 ;;	# 2 days
				"myip.ms") seconds=864000 ;;	# 10 days
				"ransomwaretracker.abuse.ch") seconds=2592000 ;;	# 30 days
				#"sslbl.abuse.ch") seconds=900 ;; # 15 min.
				#"www.openbl.org") seconds=172800 ;;	# 2 days
				"www.spamhaus.org") seconds=3600;; # 1 hours
				#*) seconds=86400;;
			esac
			
			[ "${debug}" -eq 1 ] && printf "nb seconds: %s \n" "${seconds}" >> "${bz_log}"

			if [ -f "${filename}" ]; then

				# get file seconds stat
				file_seconds=$(stat -f "%m" -t "%s" "${filename}")

				# calcul diff time in seconds
				diff_sec=$(echo "${timestamp} - ${file_seconds}" | bc)

				#unset file_seconds

				if [ ${diff_sec} -gt ${seconds} ]; then download; fi

			else

				download

			fi

			case "${ndd}" in
				"www.openbl.org")
					uncompress
				
					filename="${filename%.gz}"
				;;
			esac
			
			purge_files

			make_uniq_list

			unset filename name ndd file
            
		done
			
		) | dialog --backtitle "${ttl_project_name}" --title "${ttl_manager}" --gauge "${txt_wait}" 10 100 

    else
		[ "${debug}" -eq 1 ] && printf "%s \n" "### ${ttl_manager}: ${txt_dlg_ko}${txt_error_no_data} " >> "${bz_log}"
        dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_manager}" --infobox "${txt_dlg_ko}${txt_error_no_data}" 7 100
        sleep 1
        
        byebye

    fi
    
    sleep 1

    unset count

}

transformer_bi4d() {
	
	[ "${debug}" -eq 1 ] && printf "%s \n" "# ${ttl_transformer}: ${txt_transform_file}...\n\n ${txt_wait_minutes}" >> "${bz_log}"
	dialog --backtitle "${ttl_project_name}" --title "${ttl_transformer}" --infobox "${txt_transform_file} \n\n ${txt_wait_minutes}" 10 100 
	
	count="${#FILES[@]}"
	modulo=$(echo "100/$count"|bc)
	[ "${debug}" -eq 1 ] && printf "%s %s | %s \n" "# transformer:" "count: $count" "modulo: $modulo" >> "${bz_log}"
	
	(
	
	for f in "${FILES[@]}"; do
		
		if [ -f "${f}" ]; then
		
			let x+=$modulo
				
			cat <<EOF
XXX
$x
$txt_transform $list ($x%)
$txt_file $f
XXX
EOF
			
			output="$(basename "${f}")"

			if mv "${f}" "${DIR_LISTS}/${output}"; then 
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "OK" "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}" >> "${bz_log}"
				display_mssg "OK" "${txt_file}'${DIR_LISTS}/${output}'${txt_builded}"
			
			else
				[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_move_file}'${DIR_LISTS}/${output}'" >> "${bz_log}"
				display_mssg "KO" "${txt_error_move_file}'${DIR_LISTS}/${output}'"
			
			fi
			
			build_sums
			
			unset output
			
		else
			[ "${debug}" -eq 1 ] && printf "%s: %s \n" "KO" "${txt_error_no_file}${f}" >> "${bz_log}"
			dialog --colors --backtitle "${ttl_project_name}" --title "${ttl_transformer}" --infobox "${txt_dlg_ko}${txt_error_no_file}${f}" 10 100
		
		fi
		
		
    done
    ) | dialog --backtitle "${ttl_project_name}" --title "${ttl_transformer}" --gauge "${txt_wait}" 10 100 

	sleep 1
	
}
